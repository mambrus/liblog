/***************************************************************************
 *   Copyright (C) 2015 by Michael Ambrus                                  *
 *   ambrmi09@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/*
 * Settings management and other sporadic functions
 */
#define _GNU_SOURCE
#include "liblog/log.h"
#include "config.h"
#include "local.h"

#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>

static const char VERSION_STR[] = PROJ_NAME " <" VERSION ">";
#define FNAME_STDERR "/dev/stderr"
#define FNAME_STDOUT "/dev/stdout"

const char *liblog_version()
{
    return VERSION_STR;
}

static struct settings settings_data = {
    .s.fname = FNAME_STDERR,
    .out = NULL,
    .s.ts = SHORT_TIME,
    .s.ptid = TID_DECO,

    .s.tosyslog = false,
    .s.sys2err = true,
    .syslog_open = false,
    .s.nsync = 0,

    .s.proc_name = settings_data.proc_name,
    .dfname = false,
    .proc_name = { 0 },
    .time_relative = true,
    .time_zero = { 0, 0 },
};
struct settings *__liblog_settings = &settings_data;

/*
 * Set relative time to "now" and mark it for use
 */
void liblog_timezero_now()
{
    gettimeofday(&settings_data.time_zero, NULL);
    settings_data.time_relative = true;
}

/*
 *  Ditto but let caller choose the relative zero-time
 */
void liblog_timezero_set(const struct timeval* time_zero) {
    settings_data.time_zero = *time_zero;
    settings_data.time_relative = true;
}

/*
 * Return copy of public part of settings
 */
struct liblog_settings liblog_settings()
{
    settings_data.s.proc_name = settings_data.proc_name;
    return settings_data.s;
}

/* Global variables that affect assure */
log_level log_ASSURE = LOG_LEVEL_WARNING;
log_level log_ASSERT = LOG_LEVEL_ERROR;

/* Replace the normal output of stderr (or previous) to a file
   whose name is given as argument and return the previous filename if
   successful.

   File will be opened for appending and created if not existing. This makes
   it suitable for both cumulative log-files and for pre-created pipes.

   If previously used file exist and isn't stderr/stout it will be closed
 */
const char *liblog_logfile(const char *fname, int sync_cntr)
{
    struct settings *settings = __liblog_settings;
    FILE *curr_out = settings->out;
    FILE *new_file = NULL;

    if (strcmp(fname, FNAME_STDERR) == 0) {
        settings->out = stderr;
        settings->s.nsync = false;

        if (settings->dfname)
            free((char *)(settings->s.fname));

        settings->s.fname = FNAME_STDERR;
        settings->dfname = false; /* Don't free on next call */
        liblog_syslog_close();    /* Close if needed */

        return settings->s.fname;
    } else if (strcmp(fname, FNAME_STDOUT) == 0) {
        settings->out = stdout;
        settings->s.nsync = false;

        if (settings->dfname)
            free((char *)(settings->s.fname));

        settings->s.fname = FNAME_STDERR;
        settings->dfname = false; /* Don't free on next call */
        liblog_syslog_close();    /* Close if needed */
        return settings->s.fname;
    }

    new_file = fopen(fname, "a+");
    if (!new_file) {
        return NULL;
    }

    if (!((strcmp(settings->s.fname, FNAME_STDERR) == 0) ||
          (strcmp(settings->s.fname, FNAME_STDOUT) == 0))) {
        fclose(curr_out);
    }

    settings->s.nsync = sync_cntr;

    settings->out = new_file;
    if (settings->dfname)
        free((char *)(settings->s.fname));
    settings->s.fname = strdup(fname);
    settings->dfname = true;

    liblog_syslog_close();
    return settings->s.fname;
}

void liblog_set_decorators(ts_t ts, ptid_t ptid)
{
    struct settings *settings = __liblog_settings;
    settings->s.ts = ts;
    settings->s.ptid = ptid;
}

/*
 * Gets log-level from environment variable if existing, sets it and returns
 * the current log_level.
 */
int liblog_getenv_loglevel(void)
{
    int log_level = DEF_LOG_LEVEL;

    if ((getenv(ENV_LOG_LEVEL) != NULL) &&
        (strlen(getenv(ENV_LOG_LEVEL)) > 0)) {
        int valid_value;

        log_level = liblog_str2loglevel(getenv(ENV_LOG_LEVEL), &valid_value);
        if (!valid_value) {
            log_level = DEF_LOG_LEVEL;
        }
    }
    return log_level;
}

log_level liblog_str2loglevel(const char *str, int *ok)
{
    log_level level = DEF_LOG_LEVEL;
    int valid = 1;

    if ((strnlen(str, NAME_MAX) == 1) &&
        (((str[0] - '0') <= LOG_LEVEL_CRITICAL) &&
         ((str[0] - '0') >= LOG_LEVEL_VERBOSE))) {
        level = atoi(str);
        goto done;
    }

    if (!strcasecmp(str, "silent")) {
        level = LOG_LEVEL_SILENT;
    } else if (!strcasecmp(str, "critical")) {
        level = LOG_LEVEL_CRITICAL;
    } else if (!strcasecmp(str, "error")) {
        level = LOG_LEVEL_ERROR;
    } else if (!strcasecmp(str, "warning")) {
        level = LOG_LEVEL_WARNING;
    } else if (!strcasecmp(str, "info")) {
        level = LOG_LEVEL_INFO;
    } else if (!strcasecmp(str, "debug")) {
        level = LOG_LEVEL_DEBUG;
    } else if (!strcasecmp(str, "verbose")) {
        level = LOG_LEVEL_VERBOSE;
    } else {
        valid = 0;
    }

done:
    if (ok) /* If provided, else ignore */
        *ok = valid;
    return level;
}

/*
 * Set a proc-name or arbitrary marker. NULL clears it and disables usage
 */
void liblog_set_process_name(const char *name)
{
    struct settings *settings = __liblog_settings;
    memset(settings->proc_name, 0, NAME_MAX + 1);

    if (!name) {
        settings->s.proc_name = NULL;
    } else {
        int slen = strnlen(name, NAME_MAX);

        if (slen == NAME_MAX) {
            fprintf(stderr,
                    "%s: Argument \"name\" is longer than max-allowed [%d]"
                    "refusing to set\n",
                    __func__, NAME_MAX);
            return;
        }

        strncpy(settings->proc_name, name, NAME_MAX);
        /* Point at inner buffer marks ready for usage */
        settings->s.proc_name = settings->proc_name;
    }
}

void liblog_set_verbosity(log_level level)
{
    liblog_filter_level = level;
}

log_level liblog_get_verbosity()
{
    return liblog_filter_level;
}

#ifdef LIBLOG_USE_SYSLOG
/*
 * Set-up to use syslog
 */
void liblog_syslog_config(bool dup_stderr)
{
    struct settings *settings = __liblog_settings;
    if (settings->s.tosyslog && (dup_stderr == settings->s.sys2err)) {
        /* Nothing to do. Early return*/
        return;
    }

    /*
     Temporary re-route if concurrent threads would to write to syslog to
     preventing it from being automatically opened, which:

      - Costs cycles (syscalls)
      - State isn't synchronized with logger and it's not desired to use
        even more syscalls to find out.
     */
    settings->s.tosyslog = false;
    if (settings->syslog_open) {
        closelog();
        settings->syslog_open = false;
    }

    if (dup_stderr) {
        openlog(settings->proc_name,
                LOG_CONS | LOG_NDELAY | LOG_NOWAIT | LOG_PERROR | LOG_PID,
                LOG_USER);
        settings->s.sys2err = true;
    } else {
        openlog(settings->proc_name,
                LOG_CONS | LOG_NDELAY | LOG_NOWAIT | LOG_PID, LOG_USER);
        settings->s.sys2err = false;
    }
    settings->syslog_open = true;
    settings->s.tosyslog = true;
}
/*
 * Revert to file-IO based logging
 */
void liblog_syslog_close()
{
    struct settings *settings = __liblog_settings;
    settings->s.tosyslog = false; /* This is correct order. See notes in
                                     liblog_syslog_config()
                                   */
    if (settings->syslog_open) {
        closelog();
        settings->syslog_open = false;
    }
}

#else

void liblog_syslog_config(bool dup_stderr)
{
    assert(
        "Build-system configuration error: LIBLOG_USE_SYSLOG. Please rapport bug!" ==
        NULL);
}
void liblog_syslog_close(bool dup_stderr)
{
    assert(
        "Build-system configuration error: LIBLOG_USE_SYSLOG. Please rapport bug!" ==
        NULL);
}
#endif
