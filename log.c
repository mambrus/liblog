/***************************************************************************
 *   Copyright (C) 2015 by Michael Ambrus                                  *
 *   ambrmi09@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#define _GNU_SOURCE
#include "liblog/log.h"
#include "config.h"
#include "local.h"

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

log_level liblog_filter_level = DEF_LOG_LEVEL;

struct timeval tv_difftime(struct timeval *tv1, struct timeval *tv2)
{
    struct timeval res;

    uint64_t t1 = tv1->tv_sec * 1000000 + tv1->tv_usec;
    uint64_t t2 = tv2->tv_sec * 1000000 + tv2->tv_usec;
    uint64_t dt = t2 - t1;

    res.tv_sec = dt / 1000000;
    res.tv_usec = dt % 1000000;

    return res;
}

/*
 * Generate a time-stamp according to preference in ts
 */
static char *timestamp_str(char *tstr, ts_t ts)
{
    struct settings *s = __liblog_settings;
    struct timeval now;
    gettimeofday(&now, NULL);
    struct tm tm;

    unsigned long usec = now.tv_usec;
    unsigned long sec = now.tv_sec;

    switch (ts) {
    case DATE_TIME:
        strftime(tstr, 80, "%y%m%d-%H%M%S", localtime_r(&(now.tv_sec), &tm));
        break;
    case VERBOSE_TIME:
        /* Precise uS-time since Epoch in numerical form */
        sprintf(tstr, "%6lu.%06lu", sec, usec);
        break;
    case SHORT_TIME:
        /* Shortened uS-time since Epoch or relative to a pre-set set time_zero
         * Wraps at 10Ks (kilo-seconds), i.e. each 2.8h */

        if (s->time_relative) {
            now = tv_difftime(&s->time_zero, &now);
            usec = now.tv_usec;
            sec = now.tv_sec;
        }
        sprintf(tstr, "%04lu.%06lu", sec % 10000, usec);
        break;
    case NO_TIME:
    default:
        break;
    }
    return tstr;
}

/*
 * Generate pid/tid string according to options
 */
static char *pidtid_str(ptid_t ptid)
{
    char *astr = NULL;

    switch (ptid) {
    case TID_DECO:
        asprintf(&astr, "%7d", gettid());
        break;
    case PID_DECO:
        asprintf(&astr, "%7d", getpid());
        break;
    case PIDTID_DECO:
        asprintf(&astr, "%7d:%-7d", getpid(), gettid());
        break;
    case NO_TIME:
    default:
        break;
    }

    return astr;
}

enum decorators { D_TIME, D_PIDTID, D_PROCNAME, D_SENTINEL };
void log_write(log_level level, const char *format, ...)
{
    struct settings *settings = __liblog_settings;
    if (!settings->out)
        settings->out = stderr;

    /* Only process this message if its level exceeds the current threshold */
    if (level >= liblog_filter_level) {
        va_list args;

        /* Passed water-mark. I.e. write the log message */
        va_start(args, format);

        /* File-IO mode: prints **decorated* log-message to stderr or
         * re-routed file. Decoration is defined by settings */
        if (!settings->s.tosyslog) {
            char *n_format;
            char buff[180] = { 0 };
            bool has_deco = false;
            char *d_str[] = {
                [D_TIME] = NULL,
                [D_PIDTID] = NULL,
                [D_PROCNAME] = NULL,
            };

            if (settings->s.ts) {
                d_str[D_TIME] = strdup(timestamp_str(buff, settings->s.ts));
                has_deco = true;
            }

            if (settings->s.ptid) {
                d_str[D_PIDTID] = pidtid_str(settings->s.ptid);
                has_deco = true;
            }

            if (settings->s.proc_name && strlen(settings->s.proc_name)) {
                d_str[D_PROCNAME] = strdup(settings->s.proc_name);
                has_deco = true;
            }

            /* Build-up decorator */
            if (has_deco) {
                memset(buff, 0, 180);
                buff[0] = '[';
                size_t i;
                for (enum decorators d = D_TIME; d < D_SENTINEL; d++) {
                    if (d_str[d]) {
                        strcat(buff, d_str[d]);
                        strcat(buff, ", ");
                    }
                }
                i = strlen(buff);
                buff[i - 2] = ']';
                buff[i - 1] = 0;
            }

            if (has_deco) {
                asprintf(&n_format, "%s: %s", buff, format);
                vfprintf(settings->out, n_format, args);
                free(n_format);
                /* Free parts */
                for (enum decorators d = D_TIME; d < D_SENTINEL; d++) {
                    if (d_str[d]) {
                        strcat(buff, d_str[d]);
                        strcat(buff, ", ");
                    }
                }
            } else {
                /* Clean and simple */
                vfprintf(settings->out, format, args);
            }

            if (!(settings->s.nsync == 0)) {
                if (settings->s.nsync < 0) {
                    fflush(settings->out);
                } else {
                    static unsigned flush_cntr = 0;
                    flush_cntr++;
                    if (!(flush_cntr % settings->s.nsync))
                        fflush(settings->out);
                }
            }
        } else {
            /* Syslog manages all decoration itself */
            int syslog_level;

            switch (level) {
            case LOG_LEVEL_VERBOSE:
            case LOG_LEVEL_DEBUG:
                syslog_level = LOG_DEBUG;
                break;

            case LOG_LEVEL_EXT:
            case LOG_LEVEL_INFO:
                syslog_level = LOG_INFO;
                break;

            case LOG_LEVEL_WARNING:
                syslog_level = LOG_WARNING;
                break;

            case LOG_LEVEL_ERROR:
                syslog_level = LOG_ERR;
                break;

            case LOG_LEVEL_CRITICAL:
                syslog_level = LOG_CRIT;
                break;

            default:
                /* Shouldn't be used, so just route it to a low level */
                syslog_level = LOG_DEBUG;
                break;
            }

            vsyslog(syslog_level | LOG_USER, format, args);
        }
        va_end(args);
    }
}
