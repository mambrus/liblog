/***************************************************************************
 *   Copyright (C) 2018 by Michael Ambrus                                  *
 *   michael@helsinova.se                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/*
 * Definitions that should not be in public / installed header
 */

#ifndef liblog_local_h
#define liblog_local_h

#include <liblog/log.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/time.h>

#ifndef UNUSED
#    define UNUSED(X) ((void)(X))
#endif

struct settings {
    struct liblog_settings s;
    FILE *out;
    int syslog_open; /* Opened/closed state */
    bool dfname;     /* Filename was assigned post-build and not a constant */
    char proc_name[NAME_MAX + 1];

    bool time_relative;       /* Time-stamp relative time_zero below where
                           relevant. Otherwise time is from Epoch */
    struct timeval time_zero; /* Time-stamp for relative times */
};

extern log_level liblog_filter_level;
extern struct settings *__liblog_settings;

#endif // liblog_local_h
