/***************************************************************************
 *   Copyright (C) 2015 by Michael Ambrus                                  *
 *   ambrmi09@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef liblog_log_h
#define liblog_log_h

#include <stdbool.h>

/**
 * Severity levels for logging functions
 */
typedef enum {
    LOG_LEVEL_VERBOSE = 0, /* Verbose level logging */
    LOG_LEVEL_DEBUG,       /* Debug level logging */
    LOG_LEVEL_INFO,        /* Information level logging */
    LOG_LEVEL_WARNING,     /* Warning level logging */
    LOG_LEVEL_ERROR,       /* Error level logging */
    LOG_LEVEL_CRITICAL,    /* Fatal error level logging */
    LOG_LEVEL_EXT,         /* Externally controlled. syslogs as info */
    LOG_LEVEL_SILENT       /* No output */
} log_level;

#define LOG_LEVEL_SYS -1 /* Obsolete - Don't use (will be removed) */
#define LOG_LEVEL(L)  LOG_LEVEL_##L

/* How to time-stamp decorate */
typedef enum {
    NO_TIME,      /* Don't decorate with time stamp */
    SHORT_TIME,   /* A grasp-able usually relative 6 by 6 digit seconds
                     format (s.uS) */
    VERBOSE_TIME, /* Numerical loss-less epoch time-stamp */
    DATE_TIME     /* Date format (HH:MM:SS etc) */
} ts_t;

/* How to pid/tid decorate */
typedef enum {
    NO_PIDTID_DECO, /* Don't decorate with pid/tid */
    TID_DECO,   /* Thread only (suitable when syslog as only pid is shown) */
    PID_DECO,   /* PID only (quite useless) */
    PIDTID_DECO /* Both pid/tid */
} ptid_t;

struct liblog_settings {
    /* Non-syslog mode: */
    const char *fname; /* Write to file-name */
    ts_t ts;           /* Time-stamp decorator option */
    ptid_t ptid;       /* Pid/tid decoration option */
    int nsync;         /*  0=buffered
                          <0= Flush file after each write (warning: decreases performance)
                          >0= Flush file after each nsync write */

    /* Syslog mode:
       Note that nothing of the above applies for syslog
     */
    bool tosyslog; /* Send everything to syslog (system decides what/how to show) */
    bool sys2err; /* If syslog, also stderr */

    /* Other: */
    const char *proc_name;
};

struct timeval;
void log_write(log_level level, const char *format, ...);
const char *liblog_logfile(const char *fname, int sync_cntr);
void liblog_set_verbosity(log_level level);
void liblog_set_decorators(ts_t, ptid_t);
log_level liblog_get_verbosity();
log_level liblog_str2loglevel(const char *str, int *ok);
int liblog_getenv_loglevel(void);
void liblog_test(void);
void liblog_syslog_config(bool dup_stderr);
void liblog_syslog_close();
void liblog_set_process_name(const char *name);
struct liblog_settings liblog_settings();
const char *liblog_version();
void liblog_timezero_now();
void liblog_timezero_set(const struct timeval *);

/* Bladerf compatible macros (too much to type though) */
#define log_verbose(...)  log_write(LOG_LEVEL_VERBOSE, "[V] " __VA_ARGS__)
#define log_debug(...)    log_write(LOG_LEVEL_DEBUG, "[D] " __VA_ARGS__)
#define log_info(...)     log_write(LOG_LEVEL_INFO, "[I] " __VA_ARGS__)
#define log_warning(...)  log_write(LOG_LEVEL_WARNING, "[W] " __VA_ARGS__)
#define log_error(...)    log_write(LOG_LEVEL_ERROR, "[E] " __VA_ARGS__)
#define log_critical(...) log_write(LOG_LEVEL_CRITICAL, "[C] " __VA_ARGS__)

/* Android translation of the macros above (smoother)*/
#define LOGV log_verbose
#define LOGD log_debug
#define LOGI log_info
#define LOGW log_warning
#define LOGE log_error

#endif //liblog_log_h
